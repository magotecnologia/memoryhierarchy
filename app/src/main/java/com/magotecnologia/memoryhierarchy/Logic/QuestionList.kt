package com.magotecnologia.memoryhierarchy.Logic

import com.magotecnologia.memoryhierarchy.Logic.Question

enum class QuestionList(val question: Question) {
    S01(question = Question("Al realizar la técnica de segmentación, se reduce el tiempo de las instrucciones individualmente", optionA = "Verdadero", optionB = "Falso", optionC = "N/A", optionD = "N/A", answer = 2)),
    S02(question = Question("En todos los ciclos de la segmentación se utilizan todas las partes del procesador", optionA = "Verdadero", optionB = "Falso", optionC = "N/A", optionD = "N/A", answer = 2)),
    S03(question = Question("Al realizar una segmentación exitosa, se mejora el rendimiento en general de las instrucciones", optionA = "Verdadero", optionB = "Falso", optionC = "N/A", optionD = "N/A", answer = 1)),
    S04(question = Question("¿Cuáles son los riegos de la segmentación?", optionA = "LOCALIZACION,DEPENDENCIA DE DATOS Y PROCESAMIENTO", optionB = "ESTRUCTURA,DEPENDENCIA DE DATOS Y PROCESAMIENTO", optionC = "ESTRUCTURA,DEPENDENCIA DE DATOS Y CONTROL", optionD = "LOCALIZACION,DEPENDENCIA DE DATOS Y CONTROL", answer = 3)),
    V01(question = Question("A diferencia de los procesadores escalares con segmentación, un procesador vectorial permite realizar operaciones con registros de otro vector", optionA = "Verdadero", optionB = "Falso", optionC = "N/A", optionD = "N/A", answer = 1)),
    V02(question = Question("Los vectores con registros admiten carga/almacenado en un registro dentro del vector permitiendo operar todas las operaciones del vector", optionA = "Verdadero", optionB = "Falso", optionC = "N/A", optionD = "N/A", answer = 2)),
    V03(question = Question("DLX es un microprocesador, este es un procesador vectorial segmentado", optionA = "Verdadero", optionB = "Falso", optionC = "N/A", optionD = "N/A", answer = 2)),
    E01(question = Question("Nombre un dispositivo de entrada", optionA = "Parlantes", optionB = "Pantalla", optionC = "Teclado", optionD = "Impresora", answer = 3)),
    E02(question = Question("Nombre un dispositivo de salida", optionA = "Pantalla", optionB = "Scanner", optionC = "Teclado", optionD = "Mouse", answer = 1)),
    E03(question = Question("El rendimiento de un ________ puede estar limitado por la ____________ de cualquiera de estas partes del camino", optionA = "sistema y duracion", optionB = "periferico y velocidad", optionC = "sistema y capacidad", optionD = "sistema y velocidad", answer = 4)),
    T01(question = Question("Se pueden considerar como un método para diseñar computadores de propósito especial para equilibrar recursos, ancho de banda de E/S y cálculo", optionA = "Caches", optionB = "Entradas", optionC = "Array sistólicos", optionD = "MIMD ", answer = 3)),
    T02(question = Question("Las computadoras _____ pueden trabajar asincrónicamente  porque son sistemas con memoria compartida que pueden ejecutar varios procesos de forma __.", optionA = "MIMD,  simultánea", optionB = "SIMD, aleatoria", optionC = "MISE, simple", optionD = "SISO, distribuida", answer = 1)),
    T03(question = Question("Para el dorado el Aumento del paralelismo es directamente proporcional con el rendimiento del procesador. La afirmación anterior es ", optionA = "Verdadero", optionB = "Falso", optionC = "N/A", optionD = "N/A", answer = 1)),
    T04(question = Question("Se esperaba que los computadores de propósito especial , tuvieran un  gran papel porque :", optionA = "Se dedicaban a ejecutar instrucciones SISD", optionB = "Reducían el coste operacional", optionC = "Permiten una mayor adquisición en el mercado.", optionD = "Ofrecen mayor rendimiento y menor coste para funciones dedicadas", answer = 4)),
    M01(question = Question("¿Qué memoria es más rápida?", optionA = "Disco Duro", optionB = "RAM", optionC = "Caché", optionD = "Virtual", answer = 3)),
    M02(question = Question("Es una política de fallo de escritura", optionA = "Sobre escritura", optionB = "Ubicar en escritura", optionC = "Correspondencia directa", optionD = "Escritura directa", answer = 2)),
    M03(question = Question("Característica que me permite la memoria virtual", optionA = "Compartimentación", optionB = "Coherencia", optionC = "Redundancia", optionD = "Transaccionalidad", answer = 1)),
    M04(question = Question("Si el tiempo de acceso a la memoria es de 1 ciclo, la penalización por fallo es de 3 ciclos y la  probabilidad de fallo es de 25%, cual es el AMAT", optionA = "4 ciclos", optionB = "1.75 ciclos", optionC = "3.25 ciclos", optionD = "2 ciclos", answer = 2))
}