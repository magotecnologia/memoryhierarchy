package com.magotecnologia.memoryhierarchy.Logic

enum class Themes(val theme:Theme) {
    Segmentación(theme = Theme("Segmentación",code = "S",maxLvl = 4)),
    Entradas(theme = Theme("Entradas E/S",code = "E",maxLvl = 3)),
    Vectoriales(theme = Theme("Procesadores Vectoriales",code = "V",maxLvl = 3)),
    Memoria(theme = Theme("Jerarquia de Memoria",code = "M",maxLvl = 4)),
    Tendencias(theme = Theme("Tendencias futuras",code = "T",maxLvl = 4)),
}