package com.magotecnologia.memoryhierarchy.Fragment

import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Html
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.magotecnologia.memoryhierarchy.R
import kotlinx.android.synthetic.main.fragment_version_info_dialog.view.*


class VersionInfoDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val activity = activity
        var builder:AlertDialog.Builder?=null
        if(activity?.baseContext!=null){
            builder = AlertDialog.Builder(activity)
        }

        val packageInfo: PackageInfo?
        val version: CharSequence?
        val messageView = View.inflate(activity, R.layout.fragment_version_info_dialog, null)

        try {
            // Obtiene el nombre de la versión
            packageInfo = activity.packageManager.getPackageInfo(activity.packageName, 0)
            //Crea el mensaje completo con el link
            version="<p>Hecho por</p><p> <a href=\"https://bitbucket.org/magotecnologia\"> Marco González  </a> </p> <p>John Ortiz</p>"+ "  © 2018 Version" +" " + packageInfo!!.versionName
            //Convierte el string anterior a Spanned
            val link: Spanned = if (Build.VERSION.SDK_INT >= 24) {
                //Para un API 24 o mayor
                Html.fromHtml(version,0) // for 24 api and more
            } else {
                //Para un API anterior al 24
                Html.fromHtml(version) // or for older api
            }
            //Da la capacidad de que el texto sea clickeable
            messageView.message.movementMethod= LinkMovementMethod.getInstance()
            //Asigna al textView el mensaje con la referencia de link
            messageView.message.text=link
        } catch (e: PackageManager.NameNotFoundException) {
            //Si no encontró el número de versión, muestra una alerta
            messageView.message.text = "No se logra obtener la version"
        }
        //Establece el builder  para el dialogo con el TextView anterior
        builder?.setTitle("Información")
                ?.setCancelable(true)
                ?.setIcon(android.R.drawable.ic_dialog_info)
                ?.setPositiveButton("OK",
                        { dialog, _ -> dialog.dismiss() })
                ?.setView(messageView)
        //Crea el dialogo
        val alertToShow:Dialog?=builder?.create()
        return alertToShow!!
    }
}
