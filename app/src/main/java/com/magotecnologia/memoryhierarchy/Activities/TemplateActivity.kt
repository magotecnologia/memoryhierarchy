package com.magotecnologia.memoryhierarchy.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.magotecnologia.memoryhierarchy.Fragment.VersionInfoDialogFragment
import com.magotecnologia.memoryhierarchy.R
import org.jetbrains.anko.intentFor

open class TemplateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_template)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.game, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
            when (item?.itemId) {   //Si es el botón de Configuración
                R.id.action_settings ->{
                    //Abre el activity de relocalizar, pasandole el usuario
                    startActivity(intentFor<ThemeActivity>())
                    true
                }
                //Si es el botón de Información
                R.id.action_info -> { val fragment =  VersionInfoDialogFragment()
                    fragment.show(fragmentManager, "VersionInfo")
                    true
                }
                /*R.id.action_results -> {
                    startActivity(intentFor<ResultActivity>())
                    true
                }*/
                //Si es otro botón
                else -> {
                    true
                }
            }

}
