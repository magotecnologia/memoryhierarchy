package com.magotecnologia.memoryhierarchy.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import com.magotecnologia.memoryhierarchy.R
import kotlinx.android.synthetic.main.app_bar_game.*
import kotlinx.android.synthetic.main.content_theme.*
import org.jetbrains.anko.intentFor

class ThemeActivity : TemplateActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_theme)
        setButtonListeners()
    }

    private fun setButtonListeners() {
        btn_theme1.setOnClickListener{
            startActivity(intentFor<GameActivity>().putExtra("Type","Segmentación"))
        }
        btn_theme2.setOnClickListener{
            startActivity(intentFor<GameActivity>().putExtra("Type","Memoria"))
        }
        btn_theme3.setOnClickListener{
            startActivity(intentFor<GameActivity>().putExtra("Type","Vectoriales"))
        }
        btn_theme4.setOnClickListener{
            startActivity(intentFor<GameActivity>().putExtra("Type","Entradas"))
        }
        btn_theme5.setOnClickListener{
            startActivity(intentFor<GameActivity>().putExtra("Type","Tendencias"))
        }
    }
}
