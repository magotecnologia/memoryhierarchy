package com.magotecnologia.memoryhierarchy.Activities

import android.os.Bundle
import android.os.SystemClock
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.app_bar_game.*
import kotlinx.android.synthetic.main.content_game.*
import com.magotecnologia.memoryhierarchy.Logic.Question
import com.magotecnologia.memoryhierarchy.Logic.QuestionList
import com.magotecnologia.memoryhierarchy.Logic.Themes
import com.magotecnologia.memoryhierarchy.R
import org.jetbrains.anko.intentFor

class GameActivity : TemplateActivity() {

    lateinit var actualQuestion: Question
    lateinit var type:String
    var lvl:Int=1
    var maxlvl=2
    var wrongTries=0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        type=this.intent.getStringExtra("Type")
        lvl=if (savedInstanceState?.getLong("lvl") != null) savedInstanceState?.getInt("lvl")  else 1
        simpleChronometer.base = if (savedInstanceState?.getLong("inicioChrono") != null) savedInstanceState?.getLong("inicioChrono")  else simpleChronometer.base
        simpleChronometer.start()
        loadLvl(lvl)
        setButtonListeners()
    }

    private fun setButtonListeners() {
        btn_optionA.setOnClickListener {
           pulsedButton(btn_optionA,1)
        }
        btn_optionB.setOnClickListener {
            pulsedButton(btn_optionB,2)
        }
        btn_optionC.setOnClickListener {
            pulsedButton(btn_optionC,3)
        }
        btn_optionD.setOnClickListener {
            pulsedButton(btn_optionD,4)
        }
    }

    private fun showQuestion(){
        txt_question.text=actualQuestion.QuestionText
        btn_optionA.text=actualQuestion.optionA
        btn_optionB.text=actualQuestion.optionB
        btn_optionC.text=actualQuestion.optionC
        btn_optionD.text=actualQuestion.optionD
    }

    private fun loadLvl(lvl:Int) {
        btn_optionA.isEnabled=true
        btn_optionB.isEnabled=true
        btn_optionC.isEnabled=true
        btn_optionD.isEnabled=true

        maxlvl=Themes.valueOf(type).theme.maxLvl
        stages.text= "$lvl / $maxlvl"
        val lvlCode=Themes.valueOf(type).theme.code+"%02d".format(lvl)
        actualQuestion= QuestionList.valueOf(lvlCode).question
        showQuestion()
    }


    private fun pulsedButton(button: Button, option:Int){
        if(verifyOption(option=option, question = actualQuestion))
        {
            wrongTries=0;
            if(lvl==maxlvl){
                val time:Long= SystemClock.elapsedRealtime()-simpleChronometer.base
                startActivity(

                        intentFor<ResultActivity>().putExtra ("tiempo",time).putExtra("Type",type))
            }
            else{
                lvl++
                loadLvl(lvl)
            }
        }
        else{
            button.isEnabled=false
            wrongTries++;
            if(wrongTries==2) {
                startActivity(
                intentFor<FailActivity>().putExtra("Type",type))
            }
        }
    }



    private fun verifyOption(option: Int, question: Question):Boolean {
        return(question.answer==option)
    }

    override fun onSaveInstanceState(b: Bundle) {
        super.onSaveInstanceState(b)
        b.putLong("inicioChrono",simpleChronometer.base)
        b.putInt("lvl",lvl)
        }

}
