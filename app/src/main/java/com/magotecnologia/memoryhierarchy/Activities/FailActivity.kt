package com.magotecnologia.memoryhierarchy.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.magotecnologia.memoryhierarchy.Logic.Themes
import com.magotecnologia.memoryhierarchy.R
import kotlinx.android.synthetic.main.activity_fail.*
import org.jetbrains.anko.intentFor

class FailActivity : TemplateActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fail)
        val type=this.intent.getStringExtra("Type")
        val themeName= Themes.valueOf(type).name
        txt_fail_message.text="Fallaste dos veces en una pregunta del tema de: $themeName , estudia un poco mas"
        btn_returnFromFail.setOnClickListener {
            startActivity(
                    intentFor<GameActivity>().putExtra("Type",type))
        }
    }
}
