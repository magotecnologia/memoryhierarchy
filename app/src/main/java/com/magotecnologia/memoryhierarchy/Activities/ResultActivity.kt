package com.magotecnologia.memoryhierarchy.Activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import com.magotecnologia.memoryhierarchy.Logic.Themes
import com.magotecnologia.memoryhierarchy.R

import kotlinx.android.synthetic.main.activity_result.*
import kotlinx.android.synthetic.main.content_result.*
import org.jetbrains.anko.intentFor

class ResultActivity : TemplateActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        val type=this.intent.getStringExtra("Type")
        val time:Long=this.intent.getLongExtra("tiempo",0)
        val themeName= Themes.valueOf(type).name
        val timeinSeconds=time/1000
        val milliseconds=time%1000
        txt_result.text="Has terminado el cuestionario de $themeName en un tiempo de $timeinSeconds.$milliseconds segundos"
        btn_returnFromResult.setOnClickListener {
            startActivity(
                    intentFor<ThemeActivity>())
        }

    }

    override fun onBackPressed() {}

}
